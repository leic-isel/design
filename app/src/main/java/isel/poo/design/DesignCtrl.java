/**
 * DesignCtrl from the control.
 *
 * @Group 4
 * @author Nuno Venâncio(45824), Helder Augusto(32398), Fábio Teixeira(39619)
 * @version 1.0
 * @since   2020-05-11
 */

package isel.poo.design;

import android.app.Activity;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.DrawFilter;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Scanner;

import isel.poo.design.model.DesignModel;
import isel.poo.design.model.Figure;
import isel.poo.design.view.DesignView;

public class DesignCtrl extends Activity {

    private final int textSize = 15;
    private final String FILENAME = "Design.txt";

    Button resetBtn, loadBtn, saveBtn;
    RadioButton lineBtn, rectBtn, pixelBtn, circleBtn;
    LinearLayout btnLinearLayout, mainLinearLayout;
    RadioGroup btnRadioGroup;

    DesignModel designModel = new DesignModel();
    DesignView designView;

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    protected void onCreate(Bundle State) {
        super.onCreate(State);

        designView = new DesignView(this);
        designView.setBackgroundColor(Color.GRAY);

        resetBtn = initButtons("RESET");
        resetBtn.setOnClickListener(ctrlBtnOnClickListener);
        loadBtn = initButtons("Load");
        loadBtn.setOnClickListener(ctrlBtnOnClickListener);
        saveBtn = initButtons("Save");
        saveBtn.setOnClickListener(ctrlBtnOnClickListener);

        lineBtn = initRadioButtons("Line", 'L');
        rectBtn = initRadioButtons("Rect", 'R');
        pixelBtn = initRadioButtons("Pixel", 'P');
        circleBtn = initRadioButtons("Circle", 'C');

        initBtnLinearLayout();
        initBtnRadioGroup();
        initMainLinearLayout();

        setContentView(mainLinearLayout);
    }

    // To avoid repeating code it has been build method to create the buttons,
    // radioButtons and LinearLayouts

    // Method to create a button
    private Button initButtons(String label) {
        Button button = new Button(this);
        button.setText(label);
        button.setTextSize(textSize);
        return button;
    }

    // Method to create the radio buttons a define selection color when button selected, in pink
    @RequiresApi(api = Build.VERSION_CODES.P)
    private RadioButton initRadioButtons(String label, char id) {
        final RadioButton radioButton = new RadioButton(this);
        radioButton.setTextSize(textSize);
        radioButton.setText(label);
        radioButton.setId(id);
        ColorStateList colorStateList = new ColorStateList(
                new int[][]{
                        new int[]{-android.R.attr.state_checked},
                        new int[]{android.R.attr.state_checked}
                },
                new int[]{
                        Color.DKGRAY
                        , Color.rgb (242,81,112),
                }
        );
        radioButton.setButtonTintList(colorStateList);
        return radioButton;
    }

    // Method to create button layout and show the buttons
    private void initBtnLinearLayout() {
        btnLinearLayout = new LinearLayout(this);
        btnLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        btnLinearLayout.addView(resetBtn);
        btnLinearLayout.addView(loadBtn);
        btnLinearLayout.addView(saveBtn);
    }

    // Method to create radio button layout, show the radio buttons and
    // select the default radio button (Line")
    private void initBtnRadioGroup() {
        btnRadioGroup = new RadioGroup(this);
        btnRadioGroup.setOrientation(LinearLayout.HORIZONTAL);
        btnRadioGroup.addView(lineBtn);
        btnRadioGroup.addView(rectBtn);
        btnRadioGroup.addView(pixelBtn);
        btnRadioGroup.addView(circleBtn);
        btnRadioGroup.check('L');
    }

    // Method that creates the main layout of the application with the insertion
    // of all child layouts.
    private void initMainLinearLayout() {
        mainLinearLayout = new LinearLayout(this);
        mainLinearLayout.setOrientation(LinearLayout.VERTICAL);
        mainLinearLayout.addView(btnLinearLayout);
        mainLinearLayout.addView(btnRadioGroup);
        mainLinearLayout.addView(designView);
    }

    // OnClick event listener to detect button press and select accordingly Load, Save or Reset,
    // and treat the possible exceptions, by showing a toast to the user and a log information
    // for debug purposes.
    View.OnClickListener ctrlBtnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v == saveBtn) {
                try {
                    save();
                } catch (IOException ex) {
                    toastMsg("Problema a gravar!");
                    logMsg("Save Button", ex);
                }
            } else if (v == loadBtn) {
                try {
                    load();
                } catch (FileNotFoundException ex) {
                    toastMsg("Efetuar save primeiro!");
                    logMsg("Load Button", ex);
                }
            } else if (v == resetBtn) {
                reset();
            }
        }
    };

    // Method that starts PrintWriter and passes it as a parameter to model method responsible
    private void save() throws FileNotFoundException {
        PrintWriter pw = new PrintWriter(
                new OutputStreamWriter(openFileOutput(
                        FILENAME,
                        MODE_PRIVATE)
                )
        );
        designModel.save(pw);
        pw.close();
    }

    // Method that starts Scanner and passes it as a parameter to model method responsible
    private void load() throws FileNotFoundException {
            Scanner in = new Scanner(openFileInput(FILENAME));
            designModel.load(in);
            designView.reload(designModel.getList());
    }

    private void reset() {
        designModel.reset();
        designView.reset();
    }

    public void toastMsg(String s){
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    public void logMsg(String s, IOException ex){
        Log.e(s, ex.getMessage());
    }

    // Method responsible to call the method to create figures, in model
    public Figure createSelectedFigure(int x, int y) {
        return designModel.createSelectedFigure(
                x,
                y,
                btnRadioGroup.getCheckedRadioButtonId()
        );
    }

    /*-----------------------------------------------------------------------------------------------------*/

    // Treatment of application state bundle to avoid loosing context
    // i.e flip the device
    @Override
    protected void onSaveInstanceState(@NonNull Bundle state) {
        super.onSaveInstanceState(state);
        state.putStringArray("figures", designModel.getSaveState());
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle state) {
        super.onRestoreInstanceState(state);
        designModel.setRestoreState(state.getStringArray("figures"));
        designView.reload(designModel.getList());
    }
    /*-----------------------------------------------------------------------------------------------------*/

}
