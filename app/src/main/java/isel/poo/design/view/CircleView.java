/**
 * CircleView from the view.
 *
 * @Group 4
 * @author Nuno Venâncio(45824), Helder Augusto(32398), Fábio Teixeira(39619)
 * @version 1.0
 * @since   2020-05-11
 */

package isel.poo.design.view;

import android.graphics.Canvas;
import isel.poo.design.model.Circle;
import isel.poo.design.model.Figure;

public class CircleView extends FigureView {

    // Constructor
    public CircleView(Figure f) {
        super(f);
    }

    // Method to draw the figure
    @Override
    void draw(Canvas canvas) {
        float x, y, radius;
        x = f.getStart().getX();
        y = f.getStart().getY();
        radius = ((Circle)f).getRadius();
        canvas.drawCircle(x,y,radius,paint);
    }

}
