/**
 * DesignView from the view.
 *
 * @Group 4
 * @author Nuno Venâncio(45824), Helder Augusto(32398), Fábio Teixeira(39619)
 * @version 1.0
 * @since   2020-05-11
 */

package isel.poo.design.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;
import android.view.View;
import java.util.LinkedList;
import isel.poo.design.DesignCtrl;
import isel.poo.design.model.Figure;

public class DesignView extends View {

    DesignCtrl ctrl;
    Figure figure;
    FigureView currFigureView;
    LinkedList<FigureView> figureViewList = new LinkedList<>();

    // ctrl field is initiated inside the constructor DesignView to be initialized
    // with the application Context passed as a parameter to the constructor
    public DesignView(DesignCtrl context) {
        super(context);
        ctrl = context;
    }

    // Method that calls onDraw by polymorphism of the Figure object and the
    // color of the drawing area
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(Color.CYAN);
        for(FigureView curr : figureViewList){
            if (curr != null) {
                curr.draw(canvas);
            }
        }
    }

    // Method with the responsibility to react the touch events in the drawing area
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        if(action == MotionEvent.ACTION_DOWN) {
            int startX = (int)event.getX();
            int startY = (int)event.getY();
            figure = ctrl.createSelectedFigure(startX, startY);
            currFigureView = FigureView.getInstance(figure);
            figureViewList.add(currFigureView);
            invalidate();
            return true;
        } else if (action == MotionEvent.ACTION_MOVE) {
            int currX = (int)event.getX();
            int currY = (int)event.getY();
            currFigureView.getFigure().setEnd(currX, currY);
            invalidate();
            return true;
        } else if (action == MotionEvent.ACTION_UP) {
            currFigureView = null;
            return true;
        }
        // if none of the previous events happens, action is return to the parent class
        else return super.onTouchEvent(event);
    }

    // Method to redraw the figures, i.e. after a load event
    public void reload(LinkedList<Figure> list){
        FigureView figureView;
        figureViewList.clear();
        for (Figure f : list) {
            figureView = FigureView.getInstance(f);
            figureViewList.add(figureView);
        }
        invalidate();
    }

    // Method to reset the drawing area
    public void reset() {
        figureViewList.clear();
        invalidate();
    }

}