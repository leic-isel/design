/**
 * RectView from the view.
 *
 * @Group 4
 * @author Nuno Venâncio(45824), Helder Augusto(32398), Fábio Teixeira(39619)
 * @version 1.0
 * @since   2020-05-11
 */

package isel.poo.design.view;

import android.graphics.Canvas;
import isel.poo.design.model.Figure;
import isel.poo.design.model.Rect;

public class RectView extends FigureView {

    // Constructor
    public RectView(Figure f) {
        super(f);
    }

    // Method to draw the figure
    @Override
    void draw(Canvas canvas) {
        float left, top, right, bottom;
        left = f.getStart().getX();
        top = f.getStart().getY();
        right = ((Rect) f).getEnd().getX();
        bottom = ((Rect) f).getEnd().getY();
        canvas.drawRect(left, top, right, bottom, paint);
    }

}
