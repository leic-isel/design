/**
 * FigureView from the view.
 *
 * @Group 4
 * @author Nuno Venâncio(45824), Helder Augusto(32398), Fábio Teixeira(39619)
 * @version 1.0
 * @since   2020-05-11
 */

package isel.poo.design.view;

import android.graphics.Canvas;
import android.graphics.Paint;
import isel.poo.design.model.Circle;
import isel.poo.design.model.Figure;
import isel.poo.design.model.Line;
import isel.poo.design.model.Pixel;
import isel.poo.design.model.Rect;

public abstract class FigureView {

    protected Figure f;

    // Constructor
    public FigureView(Figure f){
        this.f = f;
    }

    // Field to define line style and width of each child class
    Paint paint = new Paint();
    {
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(7f);
    }

    public Figure getFigure(){
        return f;
    }

    // Method to select the figure to be draw on the drawing area, accordingly to the respective model
    public static FigureView getInstance(Figure f) {

        FigureView aux = null;

        if (f instanceof Rect) {
            aux = new RectView(f);
        }else if (f instanceof Line) {
            aux = new LineView(f);
        }else if (f instanceof Pixel) {
            aux = new PixelView(f);
        }else if (f instanceof Circle) {
            aux = new CircleView(f);
        }
        return aux;

    }

    // Contract method to all classes that extends FigureView
    abstract void draw(Canvas c);

}
