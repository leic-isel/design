/**
 * LineView from the view.
 *
 * @Group 4
 * @author Nuno Venâncio(45824), Helder Augusto(32398), Fábio Teixeira(39619)
 * @version 1.0
 * @since   2020-05-11
 */

package isel.poo.design.view;

import android.graphics.Canvas;
import isel.poo.design.model.Figure;
import isel.poo.design.model.Line;

public class LineView extends FigureView {

    // Constructor
    public LineView(Figure f) {
        super(f);
    }

    // Method to draw the figure
    @Override
    void draw(Canvas canvas) {
        float startX, startY, endX, endY;
        startX = f.getStart().getX();
        startY = f.getStart().getY();
        endX = ((Line)f).getEnd().getX();
        endY = ((Line)f).getEnd().getY();
        canvas.drawLine(startX, startY, endX, endY, paint);
    }

}
