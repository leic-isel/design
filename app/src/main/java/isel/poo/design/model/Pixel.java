/**
 * Pixel from the model.
 *
 * @Group 4
 * @author Nuno Venâncio(45824), Helder Augusto(32398), Fábio Teixeira(39619)
 * @version 1.0
 * @since   2020-05-11
 */

package isel.poo.design.model;

public class Pixel extends Figure {

    // Constructor
    public Pixel(int x, int y) {
        super(x, y);
        LETTER = 'P';
    }

    // Due to the contract with Figure, these methods, are needed to be implemented
    // inside Pixel class, but with no code written, because they aren't needed.
    @Override
    public void setEnd(int x, int y) {}

    @Override
    public void from(String s) {}
    /*-----------------------------------------------------------*/
}

