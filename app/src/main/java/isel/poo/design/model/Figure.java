/**
 * Figure from the model.
 *
 * @Group 4
 * @author Nuno Venâncio(45824), Helder Augusto(32398), Fábio Teixeira(39619)
 * @version 1.0
 * @since   2020-05-11
 */

package isel.poo.design.model;

public abstract class Figure {

    //Fields common to all child classes
    protected char LETTER;
    protected Point start;

    //Figure constructor
    protected Figure(int x, int y) {
        start = new Point(x, y);
    }

    public Point getStart() {
        return start;
    }

    // Method that returns the formatted string by figure, to be written in file
    @Override
    public String toString() {
        return LETTER + " " + start.toString();
    }

    // Contract methods to all classes that extends Figure.
    // Each class is responsible to implement the action for each method
    public abstract void setEnd(int x, int y);
    public abstract void from(String s);

}
