/**
 * DesignModel from the model.
 *
 * @Group 4
 * @author Nuno Venâncio(45824), Helder Augusto(32398), Fábio Teixeira(39619)
 * @version 1.0
 * @since   2020-05-11
 */

package isel.poo.design.model;

import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;

public class DesignModel {

    private LinkedList<Figure> figuresList = new LinkedList<>();

    // Method that writes the figure in the file, using, the method toString by polymorphism of
    // each class
    // i.e C (246, 255) |94|
    public void save(PrintWriter to){
        to.println(figuresList.size());
        for(Figure f : figuresList) {
            to.println(f.toString());
        }
    }

    // Method that reads from file, the number of figures, and reads each one, to be treated
    // individually, by passing responsibility to method setLoadFiguresList
    public void load(Scanner from){
        figuresList.clear();
        int len = from.nextInt();
        from.nextLine();
        String str;
        for (int i = 0; i < len; i++) {
            str = from.nextLine(); //por exemplo - L (276,268) (886,719)
            setLoadFiguresList(str);
        }
    }

    // Method that receives the figure information in string format and splits in the following way.
    // position 0 : figureType
    // position 1 : start point
    // position 2 : end point (if exists, by grabbing the radius or end point, with the use of
    //              polymorphism accordingly to each class
    private void setLoadFiguresList(String str) {
        char figureType;
        String[] splitted = str.split("\\s+"); // i.e  {L; (276,268), (886,719)}
        figureType = splitted[0].charAt(0);
        Point start = extractPointFromStr(splitted[1]);
        Figure f = createSelectedFigure(start.getX(), start.getY(), figureType);
        if (splitted.length == 3) //pixel has no third parameter
            f.from(splitted[2]);
        figuresList.add(f);
    }

    // Method to extract the point from the string
    // Made static to allow every child class to use this resource without the need
    // to create an object in DesignModel
    public static Point extractPointFromStr (String s) {
        //s = (276,268)
        int x = Integer.parseInt(
                s.substring(
                        1,
                        s.indexOf(','))
        );
        int y = Integer.parseInt(
                s.substring(
                        s.indexOf(',')+1,
                        s.length()-1
                )
        );
        return new Point(x, y);
    }

    public LinkedList<Figure> getList(){
        return figuresList;
    }

    // Saves the application bundle state
    public String[] getSaveState(){
        String[] figures = new String[figuresList.size()];
        int i = 0;
        for (Figure f : figuresList) {
            figures[i++] = f.toString();
        }
        return figures;
    }

    //Restores the application bundle state
    public void setRestoreState(String[] s){
        String str;
        for(int i = 0; i < s.length; i++) {
            str = s[i];
            setLoadFiguresList(str);
        }
    }

    // Method that allows to create the selected figure in RadioGroup.
    // By default the Line figure is selected
    public Figure createSelectedFigure(int x, int y, int currFigureId) {
        Figure f;
        switch (currFigureId) {
            case 'R':
                f = new Rect(x,y);
                break;
            case 'P':
                f = new Pixel(x,y);
                break;
            case 'C':
                f = new Circle(x,y);
                break;
            default:
                f = new Line(x,y);
        }
        figuresList.add(f);
        return f;
    }

    // Method to reset the figures list
    public void reset() {
        figuresList.clear();
    }
}
