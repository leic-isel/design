/**
 * Circle from the model.
 *
 * @Group 4
 * @author Nuno Venâncio(45824), Helder Augusto(32398), Fábio Teixeira(39619)
 * @version 1.0
 * @since   2020-05-11
 */



package isel.poo.design.model;

public class Circle extends Figure {

    // Exclusive field of circle class
    private int radius;

    // Constructor
    public Circle(int x, int y) {
        super(x, y);
        LETTER = 'C';
    }

    // Method to calculate radius
    @Override
    public void setEnd(int x, int y) {
        double dX = start.getX()-x;
        double dY = start.getY()-y;
        radius = (int)Math.sqrt(Math.pow(dX, 2) + Math.pow(dY, 2));
    }

    public int getRadius() {
        return radius;
    }

    // Method to return the values of the figure in string format
    @Override
    public String toString() {
        return super.toString() + " |" + radius + "|";
    }

    // Method that grabs the radius value from string parameter and assigns it to radius
    @Override
    public void from(String str){
        this.radius = Integer.parseInt(str.substring(1, str.length()-1));
    }

}
