/**
 * Rect from the model.
 *
 * @Group 4
 * @author Nuno Venâncio(45824), Helder Augusto(32398), Fábio Teixeira(39619)
 * @version 1.0
 * @since   2020-05-11
 */

package isel.poo.design.model;

public class Rect extends Line {

    // Constructor
    public Rect(int x, int y) {
        super(x, y);
        LETTER = 'R';
    }

    @Override
    public Point getEnd() {
        return super.getEnd();
    }

}
