/**
 * Point from the model.
 *
 * @Group 4
 * @author Nuno Venâncio(45824), Helder Augusto(32398), Fábio Teixeira(39619)
 * @version 1.0
 * @since   2020-05-11
 */

package isel.poo.design.model;

public class Point {

    private int x;
    private int y;

    // Constructor
    public Point (int x, int y){
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    // Method to return the values of the figure in string format
    @Override
    public String toString() {
        return "(" + x + "," + y + ")";
    }

}
