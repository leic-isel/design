/**
 * Line from the model.
 *
 * @Group 4
 * @author Nuno Venâncio(45824), Helder Augusto(32398), Fábio Teixeira(39619)
 * @version 1.0
 * @since   2020-05-11
 */

package isel.poo.design.model;

public class Line extends Figure {

    // Exclusive field of circle class
    private Point end;

    // Constructor
    public Line(int x, int y) {
        super(x, y);
        LETTER = 'L';
        end = start;
    }

    public Point getEnd() {
        return end;
    }

    // Method that assigns the end point coordinates to the object
    @Override
    public void setEnd(int x, int y) {
        end = new Point(x, y);
    }

    // Method to return the values of the figure in string format
    @Override
    public String toString() {
        String str = super.toString();
        if(end != null) {
            str = str + " " + end.toString();
        }
        return str;
    }

    // Method that grabs the end point coordinates from string parameter and assigns it to end point field
    @Override
    public void from(String s) {
        this.end = DesignModel.extractPointFromStr(s);
    }

}
